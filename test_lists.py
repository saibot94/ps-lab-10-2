from lists import hello_func, LinkedList, Node

def test_hello():
    assert hello_func() == "hello world"

def test_linked_list_init():
    l = LinkedList()
    assert l.head == None

    n1 = Node(12)
    l = LinkedList(n1)
    assert n1 == l.head

def test_add_empty_list():
    l = LinkedList()
    n1 = Node(1)
    l.add(n1.value)
    assert l.head.value == n1.value

def test_add_nonempty_list():
    n1 = Node(1)
    n2 = Node(2)
    l = LinkedList(n1)
    l.add(n2.value)
    assert  l.head.value == n2.value
    assert  l.head.next_node.value == n1.value

def test_find_empty_list():
    l = LinkedList()
    assert l.find(23) == False

def test_nonempty_list():
    n1 = Node(23)
    l = LinkedList(n1)
    assert l.find(10) == False
    assert l.find(23) == True

def test_two_element_list():
    n1 = Node(23)
    l = LinkedList(n1)
    l.add(10)
    assert l.find(23) == True

def test_delete_head():
    n1 = Node(23)
    l = LinkedList(n1)
    l.delete(23)
    assert l.head == None

def test_delete_head_2_elements():
    n1 = Node(23)
    n2 = Node(34)
    l = LinkedList(n1)
    l.add(n2.value)
    l.delete(34)
    assert l.head.value == n1.value

def test_delete_tail_2_elements():
    n1 = Node(23)
    n2 = Node(34)
    l = LinkedList(n1)
    l.add(n2.value)
    l.delete(23)
    assert l.head.value == n2.value
    assert l.head.next_node == None


