
def hello_func():
    return "hello world"

class Node:
    def __init__(self, value, next_node = None):
        self.value = value
        self.next_node = next_node

class LinkedList:
    def __init__(self, head = None):
        self.head = head
    
    def add(self, value):
        """
        This function adds a new node to the head of
        the list
        """
        temp_node = Node(value)
        temp_node.next_node = self.head
        self.head = temp_node
    
    def find(self, value):
        """ 
        Find a value in my linked list
        """
        temp = self.head
        while temp is not None:
            if temp.value == value:
                return True
            temp = self.head.next_node
        return False

    def delete(self, value):
        """
        Remove an element from the list, if found
        """
        temp = self.head
        if temp is None:
            return
        elif temp.value == value:
            self.head = self.head.next_node
            del temp
        else:
            while temp is not None:
                prev = temp
                temp = temp.next_node
                if temp.value == value:
                    prev.next_node = temp.next_node
                    return

        



